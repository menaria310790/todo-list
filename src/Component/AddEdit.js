import React, { useEffect, useState } from "react";
import axios from "axios";
import { useToasts } from "react-toast-notifications";
import { Link, useLocation } from "react-router-dom";

const AddEdit = (props) => {

  const [employeeFields, setEmployeeFields] = useState({ name: '', email: '', address: '', phone: '', description: '' });
  const user_id = props.match.params.id;
  const { addToast } = useToasts();
  const location = useLocation();
  console.log('helo loc', location.pathname);

  let url = "http://localhost:3003/employees";
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer' + localStorage.getItem('token')
  }

  useEffect(() => {
    loadEmployee();
  },[]);

  const loadEmployee = () => {
    if (typeof user_id !== 'undefined') {
      axios.get(url + '/' + user_id, { headers: headers }).then(function (response) {
        var response_ = response.data;
        setEmployeeFields({ ...employeeFields, ...response_ });
      });
    }
  }

  const handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    const employeeFieldsObject = {
      ...employeeFields,
      [name]: value
    }
    setEmployeeFields(employeeFieldsObject);
  }

  const submitHandler = (event) => {
    event.preventDefault();
    var formData = { ...employeeFields };
    if(!formData.hasOwnProperty('id')) {
      axios.post(url, formData).then((response) => {
        console.log(response);
        addToast("Employee added successfully!", { appearance: 'success', autoDismiss: true });
        props.history.push('/');
      }).catch(function (error) {
        addToast(error.response.data.errors[0], { appearance: 'error', autoDismiss: true });
      })
    } else {
      axios.put(url + '/' + user_id, formData).then((response) => {
        console.log('hi res', response);
        addToast("Employee updated successfully!", { appearance: 'success', autoDismiss: true });
        props.history.push('/');
      }).catch(function (error) {
        addToast(error.response.data.errors[0], { appearance: 'error', autoDismiss: true });
      })
    }
  }

  return (
    <>
    <div className="row">
    <div className="col-md-12">
   <div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-sm-6">
            {
              location.pathname === '/add-edit' ?
              <h2>Add <b>Employee</b></h2>
              :
              <h2>Edit <b>Employee</b></h2>
            }
					</div>
				</div>
			</div>
			<form>
				<div class="modal-body">					
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="name" value={employeeFields['name']} onChange={handleChange} class="form-control" />
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="email" value={employeeFields['email']} onChange={handleChange} class="form-control" />
					</div>
					<div class="form-group">
						<label>Address</label>
						<textarea name="address" value={employeeFields['address']} onChange={handleChange} class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label>Phone</label>
						<input type="text" name="phone" value={employeeFields['phone']} onChange={handleChange} class="form-control" />
					</div>
          <div class="form-group">
						<label>Description</label>
						<textarea name="description" value={employeeFields['description']} onChange={handleChange} class="form-control"></textarea>
					</div>					
				</div>
				<div class="modal-footer">
					<Link to="/" class="btn btn-default">Cancel</Link>
					<button type="submit" onClick={submitHandler} class="btn btn-success">Add</button>
				</div>
			</form>
		</div>
	</div> 
	</div> 
	</div> 
    </>
  )
}

export default AddEdit;