import React, { useEffect, useState } from "react";
import axios from "axios";
import '../assets/custom.scss';
import { Link } from "react-router-dom";

const View = (props) => {

  const [employee, setEmployee] = useState({});
  const user_id = props.match.params.id;

  let url = 'http://localhost:3003/employees';

  const headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer' + localStorage.getItem('token')
  }

  useEffect(() => {
    getEmployeeDetail(url);
  },[]);

  function getEmployeeDetail() {
    axios.get(url + '/' + user_id, { headers: headers }).then(function (response) {
      setEmployee(response.data);
    }).catch(function (error) {
      console.log('kbkjn');
    })
  }

  return (
    <>
    <div className="row">
    <div className="col-md-12">
   <div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-sm-12 d-flex">
						<h2 className="mr-auto"><b>Employee Detail</b></h2>
            <Link to="/" className="btn btn-success">Back</Link>
					</div>
				</div>
			</div>
			<div className="row employee-detail">
        <div className="col-md-6 border-bottom mb-3">
          <div className="employee-name">
            <h5>Name</h5>
            <p>{employee.name}</p>
          </div>
        </div>
        <div className="col-md-6 border-bottom mb-3">
          <div className="email">
            <h5>Email</h5>
            <p>{employee.email}</p>
          </div>
        </div>
        <div className="col-md-6 border-bottom mb-3">
          <div className="address">
            <h5>Address</h5>
            <p>{employee.address}</p>
          </div>
        </div>
        <div className="col-md-6 mb-3 border-bottom">
          <div className="phone">
            <h5>Phone</h5>
            <p>{employee.phone}</p>
          </div>
        </div>
        <div className="col-md-6">
          <div className="description">
            <h5>description</h5>
            <p>{employee.description}</p>
          </div>
        </div>
      </div>
		</div>
	</div> 
	</div> 
	</div> 
    </>
  )
}

export default View;