import React from "react";
import { HashRouter, Switch, Route } from "react-router-dom";
import TodoList from "./Component/TodoList";
import AddEdit from "./Component/AddEdit";
import View from "./Component/View";
import { ToastProvider } from "react-toast-notifications";

const App = () => {
  return (
    <ToastProvider>
    <div className="App">
      <div class="container-xl">
      <HashRouter>
        <React.Fragment>
          <Switch>
            <Route exact path="/" component={TodoList}></Route>
            <Route exact path="/add-edit" component={AddEdit}></Route>
            <Route exact path="/add-edit/:id" component={AddEdit}></Route>
            <Route exact path="/view/:id" component={View}></Route>
          </Switch>
        </React.Fragment>
      </HashRouter>
    </div>
    </div>
    </ToastProvider>
  );
}

export default App;
