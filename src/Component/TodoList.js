import React, { useEffect, useState } from "react";
import '../assets/style.scss';
import { Link } from "react-router-dom";
import axios from "axios";
import { FaTrash } from 'react-icons/fa';
import { FaRegEdit } from 'react-icons/fa';
import { FaEye } from 'react-icons/fa';
import { FaPlusCircle } from 'react-icons/fa';
import { useToasts } from "react-toast-notifications";

const TodoList = (props) => {
  const [employee, setEmployee] = useState([]);
  const { addToast } = useToasts();

  let url = "http://localhost:3003/employees";
  
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer' + localStorage.getItem('token')
  }

  useEffect(() => {
    fetchEmployee(url);
  },[]);

  function fetchEmployee(url) {
    axios.get(url, { headers: headers }).then(function (response) {
      console.log(response);
      let employeeArray = [];
      response.data.forEach(element => {
        employeeArray.push(element);
      });
      setEmployee(employeeArray);
    }).catch(function (error) {
      console.log('jvkjhkn');
    })
  }

  const removeData = (id, event) => {
    event.preventDefault();
    axios.delete(url + '/' + id, { headers: headers }).then(function (response) {
      addToast("Employee deleted successfully!", { appearance: 'error', autoDismiss: true });
      fetchEmployee(url);
    })
  }

  return (
   <React.Fragment>
	<div class="table-responsive">
		<div class="table-wrapper custom-table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-sm-6">
						<h2>Manage <b>Employees</b></h2>
					</div>
					<div class="col-sm-6">
						<Link to="/add-edit" class="btn btn-success"><FaPlusCircle /> <span className="mt-0 mr-2">Add New Employee</span></Link>					
					</div>
				</div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Address</th>
						<th>Phone</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
          {
            employee.map((employeeObject, index) => {
              return <tr key={employeeObject.id}>
              <td>{employeeObject.name}</td>
              <td>{employeeObject.email}</td>
              <td>{employeeObject.address}</td>
              <td>{employeeObject.phone}</td>
              <td>
                <Link to={`/add-edit/${employeeObject.id}`} class="edit"><FaRegEdit  /></Link>
                <Link to={`/view/${employeeObject.id}`} class="view mx-2"><FaEye /></Link>
                <Link to="/" onClick={(event) => removeData(employeeObject.id, event)} class="delete"><FaTrash  /></Link>
              </td>
            </tr>	
            })
          }
					
				</tbody>
			</table>
		</div>
	</div>        
   </React.Fragment>
  )
}

export default TodoList;